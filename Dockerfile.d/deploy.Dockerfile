#Download base image fedora:latest
FROM quay.io/fedora/fedora:latest
USER root

#Prepare Deploy environment
RUN dnf makecache; \
dnf install copr-cli -y; \
dnf clean all; \
if id -u "deployer" >/dev/null 2>&1; then \
  echo "user exists, skip creating"; \
else \
  echo "deployer user does not exist, creating"; \
  useradd -m -c "User for deploying to Fedora Copr." -g users deployer; \
fi

USER deployer
RUN mkdir -p $HOME/.config && touch $HOME/.config/copr; printf '%s\n' '[copr-cli]' 'login = $COPR_LOGIN' 'username = karlisk' 'token = $COPR_TOKEN' 'copr_url = https://copr.fedorainfracloud.org' '# expiration date: $COPR_TOKEN_EXPIRATION' >> $HOME/.config/copr;