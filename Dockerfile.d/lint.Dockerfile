#Download base image fedora:latest
FROM quay.io/fedora/fedora:latest
USER root

#Prepare RPMLint environment
RUN dnf makecache; \
dnf install -y rpmlint; \
dnf clean all; \
if id -u "linter" >/dev/null 2>&1; then \
  echo "user exists, skip creating"; \
else \
  echo "linter user does not exist, creating"; \
  useradd -m -c "User validating RPM SPEC files." -g users linter; \
fi

USER linter