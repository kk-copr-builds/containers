#Pass Fedora verion variable from .gitlab-ci.yml
ARG FEDORA_RELEASE=${RELEASE}

#Download base image
FROM quay.io/fedora/fedora:${FEDORA_RELEASE}
USER root

#Prepare Build environment
RUN dnf makecache; \
dnf groupinstall -y "Development Tools" "RPM Development Tools"; \
dnf clean all; \
if id -u "builder" >/dev/null 2>&1; then \
  echo "user exists, skip creating"; \
else \
  echo "builder user does not exist, creating"; \
  useradd -m -c "User for building stuff." -g users builder; \
fi

USER builder
