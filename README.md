# containers

Utility containers for CI builds:
- [Build container](https://registry.gitlab.com/containers/gitlab-ci-builds/build:latest) for building RPM/SRPM
- [Deploy container](https://registry.gitlab.com/containers/gitlab-ci-builds/deploy:latest) for deploying SRPM to Copr and/or RPM to Repos
- [Lint container](https://registry.gitlab.com/containers/gitlab-ci-builds/lint:latest) for linting and validating RPM SPEC files
